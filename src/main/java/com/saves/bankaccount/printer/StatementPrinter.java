package com.saves.bankaccount.printer;

import com.saves.bankaccount.domain.models.Operation;

import java.util.List;

public interface StatementPrinter {

    void print(String accountNumber, List<Operation> operations);

}
