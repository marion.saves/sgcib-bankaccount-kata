package com.saves.bankaccount.formatter;

import com.saves.bankaccount.TestFixtures;
import com.saves.bankaccount.domain.models.Operation;
import com.saves.bankaccount.formatter.TabularStatementFormatter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class TabularStatementFormatterShould {

    private final List<Operation> operations = TestFixtures.operations;
    private final List<String> sampleFormattedOperations = TestFixtures.formattedOperations;
    private final List<String> sampleFormattedEmptyOperations = TestFixtures.formattedEmptyOperations;
    private TabularStatementFormatter tabularStatementFormatter;

    @BeforeEach
    void setUp() {
        this.tabularStatementFormatter = new TabularStatementFormatter();
    }

    @Test
    void formatAnEmptyList() {
        final List<String> formattedOperations = tabularStatementFormatter.format(List.of());
        assertEquals(formattedOperations, sampleFormattedEmptyOperations);
    }

    @Test
    void format() {
        final List<String> formattedOperations = tabularStatementFormatter.format(operations);
        assertEquals(formattedOperations, sampleFormattedOperations);
    }
}
