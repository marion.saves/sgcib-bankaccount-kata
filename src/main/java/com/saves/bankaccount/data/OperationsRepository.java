package com.saves.bankaccount.data;

import com.saves.bankaccount.domain.models.Operation;

import java.util.List;
import java.util.Optional;

public interface OperationsRepository {

    List<Operation> findAllByAccountNumber(String accountNumber);

    boolean accountExists(String accountNumber);

    Optional<Operation> findLastByAccountNumber(String accountNumber);

    void save(Operation operation);
}
