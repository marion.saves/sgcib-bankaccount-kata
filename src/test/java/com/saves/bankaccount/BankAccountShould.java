package com.saves.bankaccount;

import com.saves.bankaccount.data.OperationsRepository;
import com.saves.bankaccount.domain.exceptions.InsufficientBalanceException;
import com.saves.bankaccount.domain.accountService.AccountService;
import com.saves.bankaccount.domain.accountService.RepositoryBasedAccountService;
import com.saves.bankaccount.domain.exceptions.AccountNotFoundException;
import com.saves.bankaccount.domain.exceptions.InvalidAmountException;
import com.saves.bankaccount.formatter.StatementFormatter;
import com.saves.bankaccount.formatter.TabularStatementFormatter;
import com.saves.bankaccount.printer.ConsoleStatementPrinter;
import com.saves.bankaccount.printer.StatementPrinter;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.time.Duration;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BankAccountShould {

    final String accountNumber = TestFixtures.accountNumber;
    final BigDecimal amount = TestFixtures.amount;

    @Test
    void fulfillAllRequiredFunctionalities() throws InvalidAmountException, AccountNotFoundException, InsufficientBalanceException {
        final OperationsRepository operationsRepository = new InMemoryOperationsRepository();
        final StatementFormatter statementFormatter = new TabularStatementFormatter();
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try (final PrintStream printStream = new PrintStream(byteArrayOutputStream)) {
            final StatementPrinter statementPrinter = new ConsoleStatementPrinter(statementFormatter, printStream);
            final AccountService accountService = new RepositoryBasedAccountService(
                    operationsRepository,
                    statementPrinter,
                    new TickClock(TestFixtures.sampleClock.instant(), Duration.ofHours(1), TestFixtures.timeZone)
            );

            accountService.deposit(accountNumber, amount);
            accountService.deposit(accountNumber, amount);
            accountService.deposit(accountNumber, amount);

            accountService.withdraw(accountNumber, amount);
            accountService.withdraw(accountNumber, amount);

            accountService.printStatement(accountNumber);
            final String output = byteArrayOutputStream.toString();

            assertEquals(Stream.of(
                    "Your account: 123456",
                    "Date                          Operation Type      Amount              Balance             ",
                    "2022-09-06T00:00:00           Deposit             20,00               20,00               ",
                    "2022-09-06T01:00:00           Deposit             20,00               40,00               ",
                    "2022-09-06T02:00:00           Deposit             20,00               60,00               ",
                    "2022-09-06T03:00:00           Withdraw            20,00               40,00               ",
                    "2022-09-06T04:00:00           Withdraw            20,00               20,00               ",
                    ""
            ).collect(Collectors.joining(System.lineSeparator())), output);
        }
    }
}
