package com.saves.bankaccount.domain.accountService;

import com.saves.bankaccount.domain.exceptions.AccountNotFoundException;
import com.saves.bankaccount.domain.exceptions.InsufficientBalanceException;
import com.saves.bankaccount.domain.exceptions.InvalidAmountException;

import java.math.BigDecimal;

public interface AccountService {

    void deposit(String accountNumber, BigDecimal deposit) throws AccountNotFoundException, InvalidAmountException;

    void withdraw(String accountNumber, BigDecimal withdraw) throws AccountNotFoundException, InvalidAmountException, InsufficientBalanceException;

    void printStatement(String accountNumber) throws AccountNotFoundException;
}
