package com.saves.bankaccount.printer;

import com.saves.bankaccount.TestFixtures;
import com.saves.bankaccount.domain.models.Operation;
import com.saves.bankaccount.formatter.TabularStatementFormatter;
import com.saves.bankaccount.printer.ConsoleStatementPrinter;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ConsoleStatementPrinterShould {

    private final String accountNumber = TestFixtures.accountNumber;
    private final List<Operation> operations = TestFixtures.operations;
    private final List<String> sampleFormattedOperations = TestFixtures.formattedOperations;

    @Mock
    private PrintStream printStream;

    @Mock
    private TabularStatementFormatter statementFormatter;

    @InjectMocks
    private ConsoleStatementPrinter statementPrinter;

    @Test
    void printWhenOperationsNotEmpty() {
        when(statementFormatter.format(operations)).thenReturn(sampleFormattedOperations);

        statementPrinter.print(accountNumber, operations);

        final InOrder inOrder = inOrder(statementFormatter, printStream);
        inOrder.verify(printStream).println("Your account: " + accountNumber);
        inOrder.verify(statementFormatter).format(operations);
        inOrder.verify(printStream).println(sampleFormattedOperations.get(0));
        inOrder.verify(printStream).println(sampleFormattedOperations.get(1));
        inOrder.verify(printStream).println(sampleFormattedOperations.get(2));
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    void printWhenOperationsIsEmpty() {
        final List<Operation> operationListEmpty = new ArrayList<>();
        final List<String> stringListEmpty = new ArrayList<>();

        when(statementFormatter.format(operationListEmpty)).thenReturn(stringListEmpty);

        statementPrinter.print(accountNumber, operationListEmpty);

        final InOrder inOrder = inOrder(statementFormatter, printStream);
        inOrder.verify(printStream).println("Your account: " + accountNumber);
        inOrder.verify(statementFormatter).format(operationListEmpty);
        inOrder.verify(printStream).println("No operations yet");
        inOrder.verifyNoMoreInteractions();
    }
}
