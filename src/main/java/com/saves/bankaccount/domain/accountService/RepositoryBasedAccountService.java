package com.saves.bankaccount.domain.accountService;

import com.saves.bankaccount.domain.exceptions.AccountNotFoundException;
import com.saves.bankaccount.domain.exceptions.InsufficientBalanceException;
import com.saves.bankaccount.domain.exceptions.InvalidAmountException;
import com.saves.bankaccount.data.OperationsRepository;
import com.saves.bankaccount.domain.models.Operation;
import com.saves.bankaccount.domain.models.OperationType;
import com.saves.bankaccount.printer.StatementPrinter;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Clock;
import java.time.LocalDateTime;
import java.util.List;

public class RepositoryBasedAccountService implements AccountService {


    private final OperationsRepository operationsRepository;

    private final StatementPrinter statementPrinter;

    private final Clock clock;

    public RepositoryBasedAccountService(final OperationsRepository operationsRepository, final StatementPrinter statementPrinter) {
        this.operationsRepository = operationsRepository;
        this.statementPrinter = statementPrinter;
        clock = Clock.systemDefaultZone();
    }

    public RepositoryBasedAccountService(final OperationsRepository operationsRepository,
                                         final StatementPrinter statementPrinter,
                                         final Clock clock) {
        this.operationsRepository = operationsRepository;
        this.statementPrinter = statementPrinter;
        this.clock = clock;
    }

    public void deposit(final String accountNumber, final BigDecimal amount) throws AccountNotFoundException, InvalidAmountException {
        if (!operationsRepository.accountExists(accountNumber)) {
            throw new AccountNotFoundException();
        }
        if (amount.compareTo(BigDecimal.ZERO) < 0) {
            throw new InvalidAmountException();
        }
        final BigDecimal actualBalance = operationsRepository.findLastByAccountNumber(accountNumber)
                .map(Operation::balance)
                .orElse(BigDecimal.ZERO);
        final Operation operation = new Operation(accountNumber, LocalDateTime.now(clock), OperationType.DEPOSIT, amount.setScale(2, RoundingMode.HALF_DOWN), actualBalance.add(amount).setScale(2, RoundingMode.HALF_DOWN));
        operationsRepository.save(operation);
    }

    public void withdraw(final String accountNumber, final BigDecimal amount) throws AccountNotFoundException, InvalidAmountException, InsufficientBalanceException {
        if (!operationsRepository.accountExists(accountNumber)) {
            throw new AccountNotFoundException();
        }
        if (amount.compareTo(BigDecimal.ZERO) < 0) {
            throw new InvalidAmountException();
        }
        final BigDecimal actualBalance = operationsRepository.findLastByAccountNumber(accountNumber)
                .map(Operation::balance)
                .orElse(BigDecimal.ZERO);
        if (actualBalance.subtract(amount).compareTo(BigDecimal.ZERO) < 0) {
            throw new InsufficientBalanceException();
        }
        final Operation operation = new Operation(accountNumber, LocalDateTime.now(clock), OperationType.WITHDRAW, amount.setScale(2, RoundingMode.HALF_DOWN), actualBalance.subtract(amount).setScale(2, RoundingMode.HALF_DOWN));
        operationsRepository.save(operation);
    }

    public void printStatement(final String accountNumber) throws AccountNotFoundException {
        if (!operationsRepository.accountExists(accountNumber)) {
            throw new AccountNotFoundException();
        }
        final List<Operation> operations = operationsRepository.findAllByAccountNumber(accountNumber);
        statementPrinter.print(accountNumber, operations);
    }
}
