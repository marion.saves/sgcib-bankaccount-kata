package com.saves.bankaccount;

import com.saves.bankaccount.data.OperationsRepository;
import com.saves.bankaccount.domain.models.Operation;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class InMemoryOperationsRepository implements OperationsRepository {

    final private List<Operation> operations = new ArrayList<>();

    @Override
    public List<Operation> findAllByAccountNumber(final String accountNumber) {
        return operations;
    }

    @Override
    public boolean accountExists(final String accountNumber) {
        return true;
    }

    @Override
    public Optional<Operation> findLastByAccountNumber(final String accountNumber) {
        return operations.stream().reduce((operation, operation2) -> operation2);
    }

    @Override
    public void save(final Operation operation) {
        operations.add(operation);
    }
}
