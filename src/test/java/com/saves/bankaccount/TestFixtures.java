package com.saves.bankaccount;

import com.saves.bankaccount.domain.models.Operation;
import com.saves.bankaccount.domain.models.OperationType;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;

public final class TestFixtures {
    public static final ZoneId timeZone = ZoneId.of("Europe/Paris");
    public static final Clock sampleClock = Clock.fixed(
            Instant.ofEpochMilli(1662415200000L),
            timeZone
    );
    public static final String accountNumber = "123456";
    public static final String notAnAccountNumber = "1999";
    public static final LocalDateTime sampleDate = LocalDateTime.now(sampleClock);
    public static final BigDecimal amount = new BigDecimal(20).setScale(2, RoundingMode.HALF_DOWN);
    public static final BigDecimal negativeAmount = new BigDecimal(-20).setScale(2, RoundingMode.HALF_DOWN);
    public static final BigDecimal bigAmount = new BigDecimal(5000).setScale(2, RoundingMode.HALF_DOWN);
    public static final DecimalFormat decimalFormat = new DecimalFormat("#.00");
    public static final Operation depositOperation = new Operation(accountNumber, sampleDate, OperationType.DEPOSIT, amount, new BigDecimal("100.50"));
    public static final Operation withdrawOperation = new Operation(accountNumber, sampleDate, OperationType.WITHDRAW, amount, new BigDecimal("100.50"));
    public static final List<Operation> operations = List.of(depositOperation, withdrawOperation);
    public static final List<String> formattedOperations = List.of(
            String.format("%-30s%-20s%-20s%-20s", "Date", "Operation Type", "Amount", "Balance"),
            String.format("%-30s%-20s%-20s%-20s", depositOperation.date().format(DateTimeFormatter.ISO_DATE_TIME), "Deposit", decimalFormat.format(depositOperation.amount()), decimalFormat.format(depositOperation.balance())),
            String.format("%-30s%-20s%-20s%-20s", withdrawOperation.date().format(DateTimeFormatter.ISO_DATE_TIME), "Withdraw", decimalFormat.format(withdrawOperation.amount()), decimalFormat.format(withdrawOperation.balance()))
    );
    public static final List<String> formattedEmptyOperations = List.of(
            String.format("%-30s%-20s%-20s%-20s", "Date", "Operation Type", "Amount", "Balance")
    );
    public static final Operation operationBeforeDeposit = new Operation(accountNumber, sampleDate, OperationType.DEPOSIT, amount, new BigDecimal("80.50"));
    public static final Operation operationBeforeWithdraw = new Operation(accountNumber, sampleDate, OperationType.DEPOSIT, amount, new BigDecimal("120.50"));
}
