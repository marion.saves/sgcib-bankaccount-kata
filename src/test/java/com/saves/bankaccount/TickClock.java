package com.saves.bankaccount;

import java.time.*;
import java.time.temporal.TemporalAmount;

public class TickClock extends Clock {

    private final Instant reference;
    private final TemporalAmount tick;
    private final ZoneId zoneId;

    private Instant actual;

    public TickClock(final Instant reference) {
        this(reference, Duration.ofDays(1L), ZoneOffset.UTC);
    }

    public TickClock(final Instant reference, final TemporalAmount tick, final ZoneId zoneId) {
        this.reference = reference;
        this.tick = tick;
        this.zoneId = zoneId;
    }

    @Override
    public ZoneId getZone() {
        return zoneId;
    }

    @Override
    public Clock withZone(final ZoneId zone) {
        return this;
    }

    @Override
    public Instant instant() {
        final var newActual = this.actual == null ? this.reference : this.actual.plus(tick);
        this.actual = newActual;
        return newActual;
    }
}
