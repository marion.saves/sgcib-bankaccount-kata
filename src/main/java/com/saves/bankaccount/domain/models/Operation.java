package com.saves.bankaccount.domain.models;

import java.math.BigDecimal;

public record Operation(String accountNumber, java.time.LocalDateTime date, OperationType operationType,
                        BigDecimal amount, BigDecimal balance) {
}