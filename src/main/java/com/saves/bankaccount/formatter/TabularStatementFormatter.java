package com.saves.bankaccount.formatter;

import com.saves.bankaccount.domain.models.Operation;
import com.saves.bankaccount.domain.models.OperationType;

import java.text.DecimalFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class TabularStatementFormatter implements StatementFormatter {

    public List<String> format(final List<Operation> operations) {
        final List<String> formattedOperations = new ArrayList<>();

        formattedOperations.add(String.format("%-30s%-20s%-20s%-20s", "Date", "Operation Type", "Amount", "Balance"));
        final DecimalFormat decimalFormat = new DecimalFormat("#.00");

        for (final Operation operation : operations) {
            formattedOperations.add(String.format("%-30s%-20s%-20s%-20s", operation.date().format(DateTimeFormatter.ISO_DATE_TIME), operationTypeToLabel(operation.operationType()), decimalFormat.format(operation.amount()), decimalFormat.format(operation.balance())));
        }
        return formattedOperations;
    }

    private String operationTypeToLabel(final OperationType operationType) {
        return switch (operationType) {
            case DEPOSIT -> "Deposit";
            case WITHDRAW -> "Withdraw";
        };
    }
}
