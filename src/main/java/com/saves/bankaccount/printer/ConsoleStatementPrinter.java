package com.saves.bankaccount.printer;

import com.saves.bankaccount.domain.models.Operation;
import com.saves.bankaccount.formatter.StatementFormatter;

import java.io.PrintStream;
import java.util.List;

public class ConsoleStatementPrinter implements StatementPrinter {

    private final StatementFormatter statementFormatter;

    private final PrintStream printStream;

    public ConsoleStatementPrinter(final StatementFormatter statementFormatter, final PrintStream printStream) {
        this.statementFormatter = statementFormatter;
        this.printStream = printStream;
    }

    public void print(final String accountNumber, final List<Operation> operations) {
        printStream.println("Your account: " + accountNumber);
        final List<String> formattedOperations = statementFormatter.format(operations);
        if (formattedOperations.isEmpty()) {
            printStream.println("No operations yet");
        } else {
            for (final String formattedOperation : formattedOperations) {
                printStream.println(formattedOperation);
            }
        }
    }
}
