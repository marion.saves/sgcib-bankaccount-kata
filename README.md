# README - Bank account kata

## Requirements

* Deposit and Withdrawal
* Account statement (date, amount, balance)
* Statement printing

The expected result is a service API, and its underlying implementation, that meets the expressed needs.
Nothing more, especially no UI, no persistence.

## US

### US 1
In order to save money
As a bank client
I want to make a deposit in my account

Accessible via deposit method.

### US 2
In order to retrieve some or all of my savings
As a bank client
I want to make a withdrawal from my account

Accessible via withdraw method.

### US 3
In order to check my operations
As a bank client
I want to see the history (data, date, amount, balance) of my operations

Accessible via printStatement method.