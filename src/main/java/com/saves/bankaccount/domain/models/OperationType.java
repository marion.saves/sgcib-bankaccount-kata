package com.saves.bankaccount.domain.models;

public enum OperationType {
    DEPOSIT,
    WITHDRAW
}
