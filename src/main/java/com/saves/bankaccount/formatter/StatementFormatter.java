package com.saves.bankaccount.formatter;

import com.saves.bankaccount.domain.models.Operation;

import java.util.List;

public interface StatementFormatter {

    List<String> format(List<Operation> operations);

}
