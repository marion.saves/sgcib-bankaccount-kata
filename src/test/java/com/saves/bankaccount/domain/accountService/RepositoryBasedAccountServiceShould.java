package com.saves.bankaccount.domain.accountService;

import com.saves.bankaccount.TestFixtures;
import com.saves.bankaccount.data.OperationsRepository;
import com.saves.bankaccount.domain.accountService.RepositoryBasedAccountService;
import com.saves.bankaccount.domain.exceptions.AccountNotFoundException;
import com.saves.bankaccount.domain.exceptions.InsufficientBalanceException;
import com.saves.bankaccount.domain.exceptions.InvalidAmountException;
import com.saves.bankaccount.domain.models.Operation;
import com.saves.bankaccount.printer.ConsoleStatementPrinter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.Clock;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class RepositoryBasedAccountServiceShould {

    private final Clock sampleClock = TestFixtures.sampleClock;
    private final String accountNumber = TestFixtures.accountNumber;
    private final String notAnAccountNumber = TestFixtures.notAnAccountNumber;
    private final BigDecimal amount = TestFixtures.amount;
    private final BigDecimal negativeAmount = TestFixtures.negativeAmount;
    private final BigDecimal bigAmount = TestFixtures.bigAmount;
    private final Operation depositOperation = TestFixtures.depositOperation;
    private final Operation withdrawOperation = TestFixtures.withdrawOperation;
    private final Operation operationBeforeDeposit = TestFixtures.operationBeforeDeposit;
    private final Operation operationBeforeWithdraw = TestFixtures.operationBeforeWithdraw;
    private final List<Operation> operations = TestFixtures.operations;

    @Mock
    private OperationsRepository operationsRepository;

    @Mock
    private ConsoleStatementPrinter statementPrinter;

    private RepositoryBasedAccountService accountService;

    @BeforeEach
    void setUp() {
        this.accountService = new RepositoryBasedAccountService(operationsRepository, statementPrinter, sampleClock);
    }

    @Test
    void acceptDepositWhenAccountExistAndPositiveAmount() throws InvalidAmountException, AccountNotFoundException {
        when(operationsRepository.accountExists(accountNumber)).thenReturn(true);
        when(operationsRepository.findLastByAccountNumber(accountNumber)).thenReturn(Optional.of(operationBeforeDeposit));

        accountService.deposit(accountNumber, depositOperation.amount());
        assertEquals(depositOperation.balance(), operationBeforeDeposit.balance().add(depositOperation.amount()));

        final InOrder inOrder = inOrder(operationsRepository);
        inOrder.verify(operationsRepository).accountExists(accountNumber);
        inOrder.verify(operationsRepository).findLastByAccountNumber(accountNumber);
        inOrder.verify(operationsRepository).save(depositOperation);
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    void throwAccountNotFoundExceptionWhenDepositMadeWithBadAccountNumber() {
        when(operationsRepository.accountExists(notAnAccountNumber)).thenReturn(false);

        assertThrows(AccountNotFoundException.class, () -> accountService.deposit(notAnAccountNumber, amount));

        final InOrder inOrder = inOrder(operationsRepository);
        inOrder.verify(operationsRepository).accountExists(notAnAccountNumber);
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    void throwInvalidAmountExceptionWhenDepositMadeWithNegativeAmount() {
        when(operationsRepository.accountExists(accountNumber)).thenReturn(true);

        assertThrows(InvalidAmountException.class, () -> accountService.deposit(accountNumber, negativeAmount));

        final InOrder inOrder = inOrder(operationsRepository);
        inOrder.verify(operationsRepository).accountExists(accountNumber);
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    void acceptWithdrawWhenAccountExistPositiveAmountAndSufficientBalance() throws InvalidAmountException, InsufficientBalanceException, AccountNotFoundException {
        when(operationsRepository.accountExists(accountNumber)).thenReturn(true);
        when(operationsRepository.findLastByAccountNumber(accountNumber)).thenReturn(Optional.of(operationBeforeWithdraw));

        accountService.withdraw(accountNumber, withdrawOperation.amount());
        assertEquals(withdrawOperation.balance(), operationBeforeWithdraw.balance().subtract(withdrawOperation.amount()));

        final InOrder inOrder = inOrder(operationsRepository);
        inOrder.verify(operationsRepository).accountExists(accountNumber);
        inOrder.verify(operationsRepository).findLastByAccountNumber(accountNumber);
        inOrder.verify(operationsRepository).save(withdrawOperation);
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    void throwInvalidAmountExceptionWhenWithdrawMadeWithNegativeAmount() {
        when(operationsRepository.accountExists(accountNumber)).thenReturn(true);

        assertThrows(InvalidAmountException.class, () -> accountService.withdraw(accountNumber, negativeAmount));

        final InOrder inOrder = inOrder(operationsRepository);
        inOrder.verify(operationsRepository).accountExists(accountNumber);
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    void throwNotEnoughInAccountExceptionWhenInsufficentBalance() {
        when(operationsRepository.accountExists(accountNumber)).thenReturn(true);

        assertThrows(InsufficientBalanceException.class, () -> accountService.withdraw(accountNumber, bigAmount));

        final InOrder inOrder = inOrder(operationsRepository);
        inOrder.verify(operationsRepository).accountExists(accountNumber);
        inOrder.verify(operationsRepository).findLastByAccountNumber(accountNumber);
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    void throwAccountNotFoundExceptionWhenWithdrawMadeWithBadAccountNumber() {
        when(operationsRepository.accountExists(notAnAccountNumber)).thenReturn(false);

        assertThrows(AccountNotFoundException.class, () -> accountService.withdraw(notAnAccountNumber, amount));

        final InOrder inOrder = inOrder(operationsRepository);
        inOrder.verify(operationsRepository).accountExists(notAnAccountNumber);
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    void printStatement() throws AccountNotFoundException {
        when(operationsRepository.accountExists(accountNumber)).thenReturn(true);
        when(operationsRepository.findAllByAccountNumber(accountNumber)).thenReturn(operations);

        accountService.printStatement(accountNumber);

        final InOrder inOrder = inOrder(operationsRepository, statementPrinter);
        inOrder.verify(operationsRepository).accountExists(accountNumber);
        inOrder.verify(operationsRepository).findAllByAccountNumber(accountNumber);
        inOrder.verify(statementPrinter).print(accountNumber, operations);
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    void throwAccountNotFoundExceptionWhenOperationsCheckedWithBadAccountNumber() {
        when(operationsRepository.accountExists(notAnAccountNumber)).thenReturn(false);

        assertThrows(AccountNotFoundException.class, () -> accountService.printStatement(notAnAccountNumber));

        final InOrder inOrder = inOrder(operationsRepository);
        inOrder.verify(operationsRepository).accountExists(notAnAccountNumber);
        inOrder.verifyNoMoreInteractions();
    }
}
